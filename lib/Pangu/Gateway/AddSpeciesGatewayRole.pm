package Pangu::Gateway::AddSpeciesGatewayRole v0.1.0;

use Modern::Perl '2015';
use Moose::Role;

requires 'storeSpecies';
requires 'storeEvent';
requires 'hasSpeciesWithName';
requires 'hasSpeciesWithId';

1;
