package Pangu::Interactor::AddSpeciesInteractor v0.1.0;

use Modern::Perl '2015';
use Moose;

use MooseX::Params::Validate;
use Pangu::Contract::AddSpeciesRequest;
use Pangu::Contract::AddSpeciesResponse;
use Pangu::Entity::Species;
use Pangu::Event::SpeciesAddedEvent;
use Pangu::Gateway::AddSpeciesGatewayRole;
use Pangu::Utility::IdGeneratorRole;

has 'idGenerator' => (
   is       => 'ro',
   isa      => 'Pangu::Utility::IdGeneratorRole',
   required => 1
);

has 'gateway' => (
   is       => 'ro',
   isa      => 'Pangu::Gateway::AddSpeciesGatewayRole',
   required => 1
);

sub execute
{
   my ($self, $request, $response) = validated_list(
      \@_,
      request  => { isa => 'Pangu::Contract::AddSpeciesRequest' },
      response => { isa => 'Pangu::Contract::AddSpeciesResponse' }
   );

   $response->success(0);

   my $speciesName    = $request->speciesName;
   my $relatedSpecies = $request->relatedSpecies;

   if ( ! $self->assertSpeciesNameAvailable(speciesName => $speciesName)) {
      $response->errorMessage("Species name $speciesName is already in use.");
      return;
   }

   if ( ! $self->assertAllRelatedSpeciesExist(relatedSpecies => $relatedSpecies)) {
      $response->errorMessage('One or more related species names are invalid.');
      return;
   }

   my $userId    = $request->userId;
   my $eventId   = $self->idGenerator->generate();
   my $speciesId = $self->idGenerator->generate();

   my $species = Pangu::Entity::Species->new(
      id             => $speciesId,
      name           => $speciesName,
      description    => $request->speciesDescription,
      tags           => $request->speciesTags,
      owner          => $userId,
      relatedSpecies => $relatedSpecies
   );

   my $event = Pangu::Event::SpeciesAddedEvent->new(
      id        => $eventId,
      userId    => $userId,
      speciesId => $species->id,
      species   => $species
   );

   $self->gateway->storeSpecies(species => $species);
   $self->gateway->storeEvent(event => $event);

   $response->success(1);
   $response->speciesId($speciesId);

   return;
}

sub assertAllRelatedSpeciesExist
{
   my ($self, $relatedSpecies) = validated_list(
      \@_,
      relatedSpecies => { isa => 'HashRef[Str]' }
   );

   if (%{$relatedSpecies}) {
      for my $relatedSpeciesId (keys %{$relatedSpecies}) {
         if ( ! $self->gateway->hasSpeciesWithId(speciesId => $relatedSpeciesId)) {
            return 0;
         }
      }
   }

   return 1;
}

sub assertSpeciesNameAvailable
{
   my ($self, $speciesName) = validated_list(
      \@_,
      speciesName => { isa => 'Str' }
   );

   if ($self->gateway->hasSpeciesWithName(speciesName => $speciesName)) {
      return 0;
   }

   return 1;
}

1;
