package Pangu::Entity::Species v0.1.0;

use Modern::Perl '2015';
use Moose;

has 'id' => (
   is       => 'rw',
   isa      => 'Str',
   required => 1
);

has 'name' => (
   is       => 'rw',
   isa      => 'Str',
   required => 1
);

has 'description' => (
   is       => 'rw',
   isa      => 'Str',
   required => 1
);

has 'owner' => (
   is       => 'rw',
   isa      => 'Str',
   required => 1
);

has 'relatedSpecies' => (
   is       => 'rw',
   isa      => 'HashRef[Str]',
   required => 1
);

has 'tags' => (
   is       => 'rw',
   isa      => 'ArrayRef[Str]',
   required => 1
);

1;
