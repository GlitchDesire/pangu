package Pangu::Utility::UUIDIdGenerator;

use Modern::Perl '2015';
use Moose;

use UUID::Tiny ':std';

with 'Pangu::Utility::IdGeneratorRole';

sub generate
{
   return create_uuid_as_string(UUID_V1);
}

1;
