package Pangu::Contract::AddSpeciesResponse v0.1.0;

use Modern::Perl '2015';
use Moose;

has 'success' => (
   is      => 'rw',
   isa     => 'Bool',
   default => 0
);

has 'errorMessage' => (
   is       => 'rw',
   isa      => 'Str',
   required => 0
);

has 'speciesId' => (
   is  => 'rw',
   isa => 'Str'
);

1;
