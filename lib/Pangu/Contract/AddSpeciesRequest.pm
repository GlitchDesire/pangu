package Pangu::Contract::AddSpeciesRequest v0.1.0;

use Modern::Perl '2015';
use Moose;

has 'userId' => (
   is       => 'ro',
   isa      => 'Str',
   required => 1
);

has 'speciesName' => (
   is       => 'ro',
   isa      => 'Str',
   required => 1
);

has 'speciesDescription' => (
   is       => 'ro',
   isa      => 'Str',
   required => 1
);

has 'speciesTags' => (
   is      => 'ro',
   isa     => 'ArrayRef[Str]',
   default => sub { [] }
);

has 'relatedSpecies' => (
   is      => 'ro',
   isa     => 'HashRef[Str]',
   default => sub { {} }
);

1;
