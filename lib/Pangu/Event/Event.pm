package Pangu::Event::Event v0.1.0;

use Modern::Perl '2015';
use Moose;

use DateTime;

has 'aggregator' => (
   is       => 'ro',
   isa      => 'Str',
   init_arg => undef
);

has 'aggregateId' => (
   is       => 'ro',
   isa      => 'Str',
   init_arg => 'undef'
);

has 'type' => (
   is       => 'ro',
   isa      => 'Str',
   init_arg => undef
);

has 'userId' => (
   is       => 'rw',
   isa      => 'Str',
   required => 1
);

has 'created' => (
   is       => 'ro',
   isa      => 'DateTime',
   default  => sub { DateTime->now(); },
   init_arg => undef
);

has 'data' => (
   is       => 'rw',
   isa      => 'HashRef',
   default  => sub { {} },
   init_arg => undef
);

1;
