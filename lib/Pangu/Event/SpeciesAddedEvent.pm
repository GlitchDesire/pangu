package Pangu::Event::SpeciesAddedEvent v0.1.0;

use Modern::Perl '2015';
use Moose;

use MooseX::Params::Validate;
use Pangu::Entity::Species;

extends 'Pangu::Event::Event';

has '+aggregateId' => (
   init_arg => 'speciesId'
);

has '+aggregator' => (
   default => 'species'
);

has '+type' => (
   default => 'species-added'
);

sub BUILD
{
   my ($self, $species) = validated_list(
      \@_,
      species                        => { isa => 'Pangu::Entity::Species' },
      MX_PARAMS_VALIDATE_ALLOW_EXTRA => 1
   );

   $self->data->{speciesName}        = $species->name;
   $self->data->{speciesDescription} = $species->description;
   $self->data->{relatedSpecies}     = $species->relatedSpecies;
   $self->data->{speciesTags}        = $species->tags;

   return;
}

sub speciesId
{
   my $self = shift;

   return $self->aggregateId;
}

sub speciesName
{
   my $self = shift;

   return $self->data->{speciesName};
}

sub speciesDescription
{
   my $self = shift;

   return $self->data->{speciesDescription};
}

sub speciesTags
{
   my $self = shift;

   return $self->data->{speciesTags};
}

sub relatedSpecies
{
   my $self = shift;

   return $self->data->{relatedSpecies};
}

1;
