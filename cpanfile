# cpanfile
requires 'DateTime', '1.36';
requires 'Modern::Perl', '1.20150127';
requires 'Moose', '2.18';
requires 'MooseX::Params::Validate', '0.21';
requires 'Readonly', '2.05';
requires 'UUID::Tiny', '1.04';

feature 'testing' => sub {
  requires 'Test::Class', '0.50';
  requires 'Test::More', '1.30';
}