#!/usr/bin/env perl

use Modern::Perl '2015';
use Pangu::Interactor::AddSpeciesInteractorTest;
use Pangu::Utility::UUIDIdGeneratorTest;

Test::Class->runtests;
