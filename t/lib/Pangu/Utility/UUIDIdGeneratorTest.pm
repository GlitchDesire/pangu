package Pangu::Utility::UUIDIdGeneratorTest v0.1.0;

use Modern::Perl '2015';

use Pangu::Utility::UUIDIdGenerator;
use Test::More;

use base 'Test::Class';

sub test_generate_uuid : Tests
{
   my $generator = new Pangu::Utility::UUIDIdGenerator;

   my $id = $generator->generate();

   is(length($id), 36, 'Generated ID is the correct length for a UUID.');
   like($id, qr/[[:xdigit:]]{8}(-[[:xdigit:]]{4}){3}-[[:xdigit:]]{12}/, 'Generated ID looks like a UUID.');
}

1;
