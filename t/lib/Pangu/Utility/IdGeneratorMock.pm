package Pangu::Utility::IdGeneratorMock v0.1.0;

use Modern::Perl '2015';
use Moose;

with 'Pangu::Utility::IdGeneratorRole';

has 'ids' => (
   is       => 'rw',
   isa      => 'ArrayRef[Str]',
   required => 1
);

sub generate
{
   my $self = shift;

   return shift @{$self->ids};
}

1;
