package Pangu::Interactor::AddSpeciesInteractorTest v0.1.0;

use Modern::Perl '2015';

use Pangu::Contract::AddSpeciesRequest;
use Pangu::Contract::AddSpeciesResponse;
use Pangu::Gateway::AddSpeciesGatewaySpy;
use Pangu::Interactor::AddSpeciesInteractor;
use Pangu::Utility::IdGeneratorMock;
use Readonly;
use Scalar::Util 'blessed';
use Test::More;

use base 'Test::Class';

Readonly my $EMPTY_STRING         => q();
Readonly my $EMPTY_ARRAY          => [];
Readonly my $EMPTY_HASH           => {};
Readonly my $EVENT_ID             => 'event-id';
Readonly my $EVENT_ID2            => 'event-id-2';
Readonly my $USER_ID              => 'testing-user';
Readonly my $SPECIES_ID           => 'species-id';
Readonly my $SPECIES_ID2          => 'species-id-2';
Readonly my $SPECIES_NAME         => 'Test Species';
Readonly my $SPECIES_NAME2        => 'Test Species 2';
Readonly my $SPECIES_DESCRIPTION  => 'Description for Test Species';
Readonly my $SPECIES_DESCRIPTION2 => 'Description for Test Species 2';
Readonly my $RELATED_SPECIES      => {$SPECIES_ID => 'Some relationship.'};
Readonly my $SPECIES_TAGS         => ['some', 'list', 'of', 'tags'];

sub test_add_species : Tests
{
   my $addSpeciesRequest = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME,
      speciesDescription => $SPECIES_DESCRIPTION
   );
   my $addSpeciesResponse   = Pangu::Contract::AddSpeciesResponse->new();
   my $addSpeciesGateway    = Pangu::Gateway::AddSpeciesGatewaySpy->new();
   my $addSpeciesInteractor = Pangu::Interactor::AddSpeciesInteractor->new(
      idGenerator => Pangu::Utility::IdGeneratorMock->new(
         ids => [$EVENT_ID, $SPECIES_ID]
      ),
      gateway => $addSpeciesGateway
   );

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse
   );

   my $speciesId = $addSpeciesResponse->speciesId;
   my $species   = $addSpeciesGateway->fetchSpecies(speciesId => $speciesId);
   my @events    = $addSpeciesGateway->fetchEventsForSpeciesId(speciesId => $speciesId);
   my $event     = $events[0];

   ok($addSpeciesResponse->success, 'Adding species returns successful response.');
   is($speciesId, $SPECIES_ID, 'Adding species returns a species ID.');
   is(blessed($species), 'Pangu::Entity::Species', 'Species can be retrieved by ID as an Entity.');
   is($species->owner, $USER_ID, 'Species owner is correctly stored.');
   is($species->name, $SPECIES_NAME, 'Species name is correctly stored.');
   is($species->description, $SPECIES_DESCRIPTION, 'Species description is correctly stored.');
   is_deeply($species->relatedSpecies, $EMPTY_HASH, 'Species has no related species.');
   is_deeply($species->tags, $EMPTY_ARRAY, 'Species has no tags.');

   is(blessed($event), 'Pangu::Event::SpeciesAddedEvent', 'Adding species saves a correctly-typed event.');
   is($event->aggregator, 'species', 'Species added event has correct aggregator.');
   is($event->type, 'species-added', 'Species added event has correct type.');
   is($event->userId, $USER_ID, 'Event stores user correctly.');
   is($event->speciesId, $speciesId, 'Event stores species ID correctly.');
   is($event->speciesName, $SPECIES_NAME, 'Event stores species name correctly.');
   is($event->speciesDescription, $SPECIES_DESCRIPTION, 'Event stores species description correctly.');
   is_deeply($event->relatedSpecies, $EMPTY_HASH, 'Event has no related species.');
   is_deeply($event->speciesTags, $EMPTY_ARRAY, 'Event has no species tags.');

   return;
}

sub test_add_species_with_related : Tests
{
   my $addSpeciesRequest = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME,
      speciesDescription => $SPECIES_DESCRIPTION
   );
   my $addSpeciesResponse   = Pangu::Contract::AddSpeciesResponse->new();
   my $addSpeciesGateway    = Pangu::Gateway::AddSpeciesGatewaySpy->new();
   my $addSpeciesInteractor = Pangu::Interactor::AddSpeciesInteractor->new(
      idGenerator => Pangu::Utility::IdGeneratorMock->new(
         ids => [$EVENT_ID, $SPECIES_ID, $EVENT_ID2, $SPECIES_ID2]
      ),
      gateway => $addSpeciesGateway
   );

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse
   );

   my $addSpeciesRequest2 = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME2,
      speciesDescription => $SPECIES_DESCRIPTION2,
      relatedSpecies     => $RELATED_SPECIES
   );
   my $addSpeciesResponse2 = Pangu::Contract::AddSpeciesResponse->new();

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest2,
      response => $addSpeciesResponse2
   );

   my $speciesId = $addSpeciesResponse2->speciesId;
   my $species   = $addSpeciesGateway->fetchSpecies(speciesId => $speciesId);
   my @events    = $addSpeciesGateway->fetchEventsForSpeciesId(speciesId => $speciesId);
   my $event     = $events[0];

   is_deeply($species->relatedSpecies, $RELATED_SPECIES, 'Entity correctly stores related species.');
   is_deeply($event->relatedSpecies, $RELATED_SPECIES, 'Event correctly stores related species.');

   return;
}

sub test_add_species_with_tags : Tests
{
   my $addSpeciesRequest = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME,
      speciesDescription => $SPECIES_DESCRIPTION,
      speciesTags        => $SPECIES_TAGS
   );
   my $addSpeciesResponse   = Pangu::Contract::AddSpeciesResponse->new();
   my $addSpeciesGateway    = Pangu::Gateway::AddSpeciesGatewaySpy->new();
   my $addSpeciesInteractor = Pangu::Interactor::AddSpeciesInteractor->new(
      idGenerator => Pangu::Utility::IdGeneratorMock->new(
         ids => [$EVENT_ID, $SPECIES_ID, $EVENT_ID2, $SPECIES_ID2]
      ),
      gateway => $addSpeciesGateway
   );

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse
   );

   my $speciesId = $addSpeciesResponse->speciesId;
   my $species   = $addSpeciesGateway->fetchSpecies(speciesId => $speciesId);
   my @events    = $addSpeciesGateway->fetchEventsForSpeciesId(speciesId => $speciesId);
   my $event     = $events[0];

   is_deeply($species->tags, $SPECIES_TAGS, 'Entity correctly stores tags.');
   is_deeply($event->speciesTags, $SPECIES_TAGS, 'Event correctly stores tags.');

   return;
}

sub test_add_species_with_invalid_related_should_fail : Tests
{
   my $addSpeciesRequest = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME,
      speciesDescription => $SPECIES_DESCRIPTION,
      relatedSpecies     => { 'invalid-species-name' => 'Some relation description' }
   );
   my $addSpeciesResponse   = Pangu::Contract::AddSpeciesResponse->new();
   my $addSpeciesGateway    = Pangu::Gateway::AddSpeciesGatewaySpy->new();
   my $addSpeciesInteractor = Pangu::Interactor::AddSpeciesInteractor->new(
      idGenerator => Pangu::Utility::IdGeneratorMock->new(
         ids => [$EVENT_ID, $SPECIES_ID]
      ),
      gateway => $addSpeciesGateway
   );

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse
   );

   ok( ! $addSpeciesResponse->success, 'Adding species with invalid related species correctly sets failure code.');
   isnt($addSpeciesResponse->errorMessage, $EMPTY_STRING, 'Adding species with invalid related species sets an error message.');

   return;
}

sub test_add_duplicate_species_should_fail : Tests
{
   my $addSpeciesRequest = Pangu::Contract::AddSpeciesRequest->new(
      userId             => $USER_ID,
      speciesName        => $SPECIES_NAME,
      speciesDescription => $SPECIES_DESCRIPTION
   );
   my $addSpeciesResponse   = Pangu::Contract::AddSpeciesResponse->new();
   my $addSpeciesGateway    = Pangu::Gateway::AddSpeciesGatewaySpy->new();
   my $addSpeciesInteractor = Pangu::Interactor::AddSpeciesInteractor->new(
      idGenerator => Pangu::Utility::IdGeneratorMock->new(
         ids => [$EVENT_ID, $SPECIES_ID]
      ),
      gateway => $addSpeciesGateway
   );

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse
   );

   my $addSpeciesResponse2 = Pangu::Contract::AddSpeciesResponse->new();

   $addSpeciesInteractor->execute(
      request  => $addSpeciesRequest,
      response => $addSpeciesResponse2
   );

   ok( ! $addSpeciesResponse2->success, 'Adding species twice correctly sets failure code.');
   isnt($addSpeciesResponse2->errorMessage, $EMPTY_STRING, 'Adding species twice sets an error message.');

   return;
}

1;
