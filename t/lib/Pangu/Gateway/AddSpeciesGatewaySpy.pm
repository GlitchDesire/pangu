package Pangu::Gateway::AddSpeciesGatewaySpy v0.1.0;

use Modern::Perl '2015';
use Moose;

use Data::Dumper;
use List::Util qw(any first);
use MooseX::Params::Validate;
use Pangu::Entity::Species;

with 'Pangu::Gateway::AddSpeciesGatewayRole';

has 'storedSpecies' => (
   is      => 'rw',
   isa     => 'ArrayRef[Pangu::Entity::Species]',
   default => sub { [] }
);

has 'storedEvents' => (
   is      => 'rw',
   isa     => 'ArrayRef[Pangu::Event::SpeciesAddedEvent]',
   default => sub { [] }
);

sub storeSpecies
{
   my ($self, $species) = validated_list(
      \@_,
      species => { isa => 'Pangu::Entity::Species' }
   );

   push(@{$self->storedSpecies}, $species);

   return;
}

sub storeEvent
{
   my ($self, $event) = validated_list(
      \@_,
      event => { isa => 'Pangu::Event::SpeciesAddedEvent' }
   );

   push(@{$self->storedEvents}, $event);

   return;
}

sub fetchSpecies
{
   my ($self, $speciesId) = validated_list(
      \@_,
      speciesId => { isa => 'Str' }
   );

   return first { $_->id eq $speciesId } @{$self->storedSpecies};
}

sub hasSpeciesWithName
{
   my ($self, $speciesName) = validated_list(
      \@_,
      speciesName => { isa => 'Str' }
   );

   return any { $_->name eq $speciesName } @{$self->storedSpecies};
}

sub hasSpeciesWithId
{
   my ($self, $speciesId) = validated_list(
      \@_,
      speciesId => { isa => 'Str' }
   );

   return any { $_->id eq $speciesId } @{$self->storedSpecies};
}

sub fetchEventsForSpeciesId
{
   my ($self, $speciesId) = validated_list(
      \@_,
      speciesId => { isa => 'Str' }
   );

   return grep { $_->speciesId eq $speciesId } @{$self->storedEvents};
}

1;
